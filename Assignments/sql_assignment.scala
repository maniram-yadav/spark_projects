package wipro_assignment


import org.apache.spark.{SparkConf, SparkContext}

import org.apache.spark.sql.{SQLContext,SparkSession}

/**
  * Created by maniram on 27/7/18.
  */
object sql_assignment {



  def main(args : Array[String]) {


    val session = SparkSession.builder().appName("SQL_Assignment").master("local[1]").getOrCreate()
    val data = session.read.json("in/employee.json")


    /*
      a)	Display file data using data frame.
    b)	Display the structure of data frame .
    c)	Display only enames from the given file using select.
*/

    data.show()
    data.printSchema()
    data.select("ename").show()

    /*
        a)	Provide the count of each age.
      b)	Provide all records in sorted order.
      c)	Display  records whose age <=30.
  */

    data.registerTempTable("employee")
    session.sql("select age,count(age) from employee group by age").show()
    session.sql("select * from employee order by ename asc").show()
    data.where("age<=30").show()

    session.stop()
  }

}
