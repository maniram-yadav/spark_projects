package wipro_assignment


import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by maniram on 27/7/18.
  */
object assignment_rdd_files {





  def main(args : Array[String]){

    val conf = new SparkConf().setMaster("local[1]").setAppName("Accumulator_Broadcast_Example ")

    val sc = new SparkContext(conf)



    val rdd = sc.parallelize(Seq( ("Ram",10), ("sam",30),("joe",40), ("sai",54),
      ("Ram",34),("sam",99),("joe",50),("tej",34),("Tej",39),("sai",120),("tej",45)))

    /*
a)	Provide the output in the form of keypairs using RDD.
b)	Provide aggregation of each name
c)	Provide the count of occurrence each name
*/

    rdd.foreach{case(x,y)=> println(s"(${x} , ${y})")}

    val Key_aggregrate = rdd.reduceByKey(_+_)
    val Name_Occurance = rdd.countByKey()

    println("Key aggregration--")
    Key_aggregrate.foreach{case(key,value)=>println(s" ${key}, ${value} ")}
    Name_Occurance.foreach{case(name,occurance)=>println(s" ${name}, ${occurance} ")}






    val rdd_file = sc.textFile("in/sample.txt")


    /*
a)	Display file using RDD
b)	Display first line of sample.txt.
c)	Display first 2 lines of sample.txt
d)	Identify how many words are there and provide word count.
*/
    rdd_file.collect().foreach(println)
    println(rdd_file.first())   // First Line of file
    rdd_file.top(2).foreach(println) //   Top 2 line in given file
    val wordcount =rdd_file.flatMap(x=> x.split("(\\s+)|([$.,])")).map(x => (x,1)).reduceByKey(_+_)

//total number of words
    val totalWords = wordcount.map{case(word,count)=>
      (count)
    }.reduce(_+_)
    println(s"\n Total number of words : ${totalWords}")

    wordcount.foreach{case(word,count)=>println(s" ${word} , ${count}")}    // Count word in given file




    /*
    *
a)	Display all tokens in the given file separated by a space
b)	Display all distinct words from the text files.
c)	Provide word count of given file.
    *
    */


    val tokens = rdd_file.flatMap(x => x.split(" "))   //  All token in given file
    tokens.foreach(println)
    tokens.distinct().foreach(println)
    tokens.countByValue().foreach{case(word,count) => println(s"  ${word} , ${count}")}


  sc.stop()
  }

}
