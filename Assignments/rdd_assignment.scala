package wipro_assignment


import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by maniram on 27/7/18.
  */
object rdd_assignment {





    def main(args : Array[String]) {

      val conf = new SparkConf().setMaster("local[1]").setAppName("Array_RDD_Example ")

      val sc = new SparkContext(conf)

      val rdd1 = sc.parallelize(Array(10,21,90,34,40,98,21,44,59,21,90,34,29,19, 21,90,34,29,49,78))
      rdd1.collect().foreach(print)   //Display the array
      println(rdd1.first())     // First element of array
      println()


      val rdd2 = sc.parallelize(Array(10,21,90,34,40,98,21,44,59,21,90,34,29,19, 21,90,34,29,49,78))
      print("\nDescending Order   :  ")
      rdd2.sortBy(x => x,false).foreach(item =>print("  "+item)) // Descending Order
      print("\nAscending Order   :  ")
      rdd2.sortBy(x => x,true).foreach(item =>print("  "+item))  // Ascending Order
      println("\nDistinct Element of RDD array new RDD :  ")
      val distinct = rdd2.distinct()
      distinct.collect().foreach(x => print("  "+x))     // Distinct element by using new RDD
      println()
      rdd2.distinct().foreach(num => print("  "+num))             // Distinct element without new RDD




      val rdd3 = sc.parallelize(Array(10,21,90,34,40,98,21,44,59,21,90,34,29,19, 21,90,34,29,49,78))
      val min = rdd3.min()
      val max = rdd3.max()
      print(s"\nMin value : ${min} , Max Value : ${max}") // Min and max value
      println()
      val top5 =  rdd3.top(5)
      println("\nTop 5 element ")
      top5.foreach(i => print("  "+i))     // Top 5 element of RDD
      println("\nCombined array ")
      val combinedArray = top5 ++ Array(30,35,45,60,75,85)
      combinedArray.foreach(y => print(s"  ${y}"))
      val distinctsum = combinedArray.distinct.reduce(_+_)    //  Sum of distinct element
      println("\nSum of Distinct element  :  "+distinctsum)
      val sum = combinedArray.reduce(_+_)                     // Sumof all element
      println("\nSum of All element  :  "+sum)


      sc.stop()


    }
}
