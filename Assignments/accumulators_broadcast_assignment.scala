package wipro_assignment

/**
  * Created by maniram on 27/7/18.
  */

import org.apache.spark.{SparkContext,SparkConf};


object accumulators_broadcast_assignment {

  def main(args : Array[String]){

  val conf = new SparkConf().setMaster("local[1]").setAppName("Accumulator_Broadcast_Example ")

  val sc = new SparkContext(conf)
  val broadcast = sc.broadcast(Array(10,20,30,40,50))

  var accumulator = sc.accumulator(0,"Accumulator_1");

  sc.parallelize(Array(20,25,30,35,40)).foreach( item => accumulator.add(item))


  broadcast.value.foreach(x => print(" "+x))
  println()
  println(s"Accumulator Value : ${accumulator}")

  sc.stop()

}

}
